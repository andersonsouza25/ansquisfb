package com.developer.souza.ansquiz.Models;

public class CategoriaModel {

    private Long id;
    private String nome;
    private String img;

    public CategoriaModel() {
    }

    public CategoriaModel(Long id,String nome, String img) {

        this.id =id;
        this.nome = nome;
        this.img = img;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }






}
