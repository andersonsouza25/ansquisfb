package com.developer.souza.ansquiz.Views;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.developer.souza.ansquiz.Interface.ItemClickListener;
import com.developer.souza.ansquiz.R;

public class DetalheView extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView cell_d_txt_name_categora,cell_d_txt_score_categora;
    private ItemClickListener itemClickListener;

    public ItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public DetalheView(@NonNull View itemView) {
        super( itemView );
       cell_d_txt_name_categora= itemView.findViewById( R.id.cell_d_txt_name_categora );
       cell_d_txt_score_categora=itemView.findViewById( R.id.cell_d_txt_score_categoria );

       itemView.setOnClickListener( this );


    }

    @Override
    public void onClick(View view) {

    }


}
