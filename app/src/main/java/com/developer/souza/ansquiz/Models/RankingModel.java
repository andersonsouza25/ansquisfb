package com.developer.souza.ansquiz.Models;

public class RankingModel {
    private String user_name;
    private int score_user;

    public RankingModel() {
    }

    public RankingModel(String user_name, int score_user) {
        this.user_name = user_name;
        this.score_user = score_user;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getScore_user() {
        return score_user;
    }

    public void setScore_user(int score_user) {
        this.score_user = score_user;
    }
}
