package com.developer.souza.ansquiz;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    private FrameLayout layout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        BottomNavigationView navView = findViewById( R.id.nav_view );
        layout=findViewById( R.id.frag_main );
        navView.setOnNavigationItemSelectedListener( mOnNavigationItemSelectedListener );



    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment=null;
            switch (item.getItemId()) {
                case R.id.menu_categoria:
                    fragment=CategoriaFrag.newInstance();
                    break;
                case R.id.menu_ranking:
                    fragment=RankingFrag.newInstance();
                    break;
                case R.id.menu_people:
                    fragment=Online_frag.newInstance();
                    break;
            }

            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace( R.id.frag_main,fragment );
            transaction.commit();
            return true;
        }
    };



}
