package com.developer.souza.ansquiz.Views;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.souza.ansquiz.Interface.ItemClickListener;
import com.developer.souza.ansquiz.R;


public class CategoriaView extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView nome_categoria;
    public ImageView img_categoria;

    private ItemClickListener itemClickListener;

    public ItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public CategoriaView(@NonNull View itemView) {
        super( itemView );
        nome_categoria=itemView.findViewById( R.id.txt_name_categoria );
        img_categoria=itemView.findViewById( R.id.img_categoria );


        itemView.setOnClickListener( this );

    }


    @Override
    public void onClick(View view) {

        itemClickListener.onClick( view,getAdapterPosition(), false);
    }


}
