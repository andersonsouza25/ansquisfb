package com.developer.souza.ansquiz.Models;

public class QuestionModel {

    private String question;
    private Long id_categoria;
    private int id_questao;
    private String asw_a;
    private String asw_b;

    private String asw_c;
    private String asw_d;
    private String correta;

    private boolean img;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Long getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Long id_categoria) {
        this.id_categoria = id_categoria;
    }

    public int getId_questao() {
        return id_questao;
    }

    public void setId_questao(int id_questao) {
        this.id_questao = id_questao;
    }

    public String getAsw_a() {
        return asw_a;
    }

    public void setAsw_a(String asw_a) {
        this.asw_a = asw_a;
    }

    public String getAsw_b() {
        return asw_b;
    }

    public void setAsw_b(String asw_b) {
        this.asw_b = asw_b;
    }

    public String getAsw_c() {
        return asw_c;
    }

    public void setAsw_c(String asw_c) {
        this.asw_c = asw_c;
    }

    public String getAsw_d() {
        return asw_d;
    }

    public void setAsw_d(String asw_d) {
        this.asw_d = asw_d;
    }

    public String getCorreta() {
        return correta;
    }

    public void setCorreta(String correta) {
        this.correta = correta;
    }

    public boolean isImg() {
        return img;
    }

    public void setImg(boolean img) {
        this.img = img;
    }

    public QuestionModel(String question, Long id_categoria, int id_questao, String asw_a, String asw_b, String asw_c, String asw_d, String correta, boolean img) {
        this.question = question;
        this.id_categoria = id_categoria;
        this.id_questao = id_questao;
        this.asw_a = asw_a;
        this.asw_b = asw_b;
        this.asw_c = asw_c;
        this.asw_d = asw_d;
        this.correta = correta;
        this.img = img;
    }




    public QuestionModel() {
    }



}
