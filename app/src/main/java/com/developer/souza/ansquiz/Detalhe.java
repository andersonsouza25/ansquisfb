package com.developer.souza.ansquiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.developer.souza.ansquiz.GerentePack.Gerente;
import com.developer.souza.ansquiz.Models.QuestionScoreModel;
import com.developer.souza.ansquiz.Models.RankingModel;
import com.developer.souza.ansquiz.Views.DetalheView;
import com.developer.souza.ansquiz.Views.RankingView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Detalhe extends AppCompatActivity {

    //coisas do firebase
    DatabaseReference detalhe_ref;
    FirebaseDatabase database;
    RecyclerView rcv_detalhe;
    RecyclerView.LayoutManager layoutManager;

    FirebaseRecyclerAdapter<QuestionScoreModel, DetalheView> fba_ranking_datalhe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_detalhe );

        database=FirebaseDatabase.getInstance();
        detalhe_ref=database.getReference("Question_ranking/"+Gerente.user_name+ String.valueOf(  Gerente.categoria_id)) ;

        rcv_detalhe=findViewById( R.id.rcv_detalhe_ranking );

        rcv_detalhe.setHasFixedSize( true );

        layoutManager=new LinearLayoutManager(  this );//nao sei explicar direito

        rcv_detalhe.setLayoutManager( layoutManager );

        show_detalhe();
    }

    private void show_detalhe() {
        fba_ranking_datalhe=new FirebaseRecyclerAdapter<QuestionScoreModel, DetalheView>(
                QuestionScoreModel.class,//1 - vc tem que passar o modelo que vai receber os dados do fb
                R.layout.cell_detalhe,//2- vc tem que passar o layout das celulas da view
                DetalheView.class,//3- vc tem que passar a classe view, que onde vc conectou os itens
                detalhe_ref  //4- vc passa o firebase ref para poder buscar os dados
        ) {
            @Override
            protected void populateViewHolder(DetalheView viewHolder, QuestionScoreModel model, int position) {
                //Aqui é onde vc vai setar os dados que vc trouxe do fb nas views
                viewHolder.cell_d_txt_name_categora.setText( model.getCategoria_name() );
                viewHolder.cell_d_txt_score_categora.setText( String.valueOf(  model.getScore_categoria() ));
//

            }
        };

        //Aqui é para atualizar a lista quando mudar algum dado
        fba_ranking_datalhe.notifyDataSetChanged();


        rcv_detalhe.setAdapter( fba_ranking_datalhe );
    }
}
