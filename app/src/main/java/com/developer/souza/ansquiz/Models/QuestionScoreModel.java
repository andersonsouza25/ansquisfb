package com.developer.souza.ansquiz.Models;

public class QuestionScoreModel {
    private String categoria_name;
    private int score_categoria;

    public QuestionScoreModel() {
    }

    public QuestionScoreModel(String categoria_name, int score_categoria) {
        this.categoria_name = categoria_name;
        this.score_categoria = score_categoria;
    }

    public String getCategoria_name() {
        return categoria_name;
    }

    public void setCategoria_name(String categoria_name) {
        this.categoria_name = categoria_name;
    }

    public int getScore_categoria() {
        return score_categoria;
    }

    public void setScore_categoria(int score_categoria) {
        this.score_categoria = score_categoria;
    }
}