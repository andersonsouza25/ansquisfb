package com.developer.souza.ansquiz.Views;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.developer.souza.ansquiz.Interface.ItemClickListener;
import com.developer.souza.ansquiz.R;

public class RankingView extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {
    public TextView txt_user_name,txt_user_score;
    public CardView cardView;
    private ItemClickListener itemClickListener;

    public ItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;


    }

    public RankingView(@NonNull View itemView) {
        super( itemView );

        txt_user_name=itemView.findViewById( R.id.cell_d_txt_name_categora );
        txt_user_score=itemView.findViewById(R.id.cell_d_txt_score_categoria ) ;
        cardView=itemView.findViewById( R.id.cell_r_card_view );

        itemView.setOnClickListener( this );

        itemView.setOnLongClickListener(  this );
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick( view,getAdapterPosition(),false );
    }

    @Override
    public boolean onLongClick(View view) {

        if (cardView.isLongClickable()){
            return true;
        }

        return false;


    }
}
