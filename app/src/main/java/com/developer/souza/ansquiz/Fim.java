package com.developer.souza.ansquiz;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.souza.ansquiz.GerentePack.Gerente;
import com.developer.souza.ansquiz.Models.QuestionModel;
import com.developer.souza.ansquiz.Models.QuestionScoreModel;
import com.developer.souza.ansquiz.Models.RankingModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

public class Fim extends AppCompatActivity {
    private TextView txt_fim_point;
    private Button btn_home;
    private FirebaseDatabase database;
    private DatabaseReference ranking_ref,question_ranking_ref;

    String user_ponto="0";
    int last_score_fb=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_fim );

        txt_fim_point=findViewById( R.id.ac_f_txt_ponit );
        btn_home=findViewById( R.id.ac_f_btn_home );

        btn_home.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( Fim.this,MainActivity.class );
                PlayActivity.clean_data();
                startActivity( intent );
            }
        } );

        Bundle extra=getIntent().getExtras();
        if (extra!=null)
        {
            user_ponto= String.valueOf( extra.getInt( "ponto" ));
            txt_fim_point.setText( user_ponto );
        }

        database=FirebaseDatabase.getInstance();
        ranking_ref=database.getReference("Ranking");
        question_ranking_ref=database.getReference("Question_ranking/"+Gerente.user_name);

        add_user();
    }

    private void add_user()
    {//Aqui é onde vai fazer a busca no No ranking para se tem ou nao um usuario
       question_ranking_ref.addListenerForSingleValueEvent( new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if (dataSnapshot.child( Gerente.user_name).exists() && dataSnapshot.child((String.valueOf( Gerente.categoria_id ))).exists())
               {
                   QuestionScoreModel model=dataSnapshot.child(String.valueOf( Gerente.categoria_name ) ).getValue(QuestionScoreModel.class);
                            atualiza_data_fb(model);


               }else{
                   QuestionScoreModel questionScoreModel=new QuestionScoreModel( Gerente.categoria_name,Integer.parseInt( user_ponto ));
                   question_ranking_ref.child( String.valueOf(  Gerente.categoria_id )).setValue( questionScoreModel );
               }

               verifica_score();
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       } );
    }

    private void atualiza_data_fb(QuestionScoreModel model) {

        if (model.getScore_categoria()<=Integer.parseInt(  user_ponto)){
            model.setScore_categoria( Integer.parseInt(  user_ponto ));
            question_ranking_ref.child( Gerente.user_name ).child( String.valueOf( Gerente.categoria_id ) ).setValue( model );
        }


    }

    private void verifica_score()
    {//Aqui é para verificar se o score que esta no banco é maior ou menor do que os pontos que o usuario fez na categoria jogada.


        question_ranking_ref.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //for(DataSnapshot post: dataSnapshot.child( String.valueOf( Gerente.categoria_id ) ).getChildren())
//                for(DataSnapshot post:dataSnapshot.getChildren()){
//                    Log.d( "meu", String.valueOf( last_score_fb+Integer.parseInt( String.valueOf(  post.child( "score_categoria" ).getValue()))));
//                    //Gerente.fb_scores.add( last_score_fb );
//                }
                if (dataSnapshot.child( String.valueOf(  Gerente.categoria_id )).exists()){
                    for(DataSnapshot post:dataSnapshot.getChildren()){
//                    Log.d( "meu", String.valueOf( last_score_fb+Integer.parseInt( String.valueOf(  post.child( "score_categoria" ).getValue()))));
//                    //Gerente.fb_scores.add( last_score_fb );
                        last_score_fb= last_score_fb + Integer.parseInt(  post.child( "score_categoria" ).getValue().toString());

                }
                }else{
                    Log.d( "meu","Nãoooooooooo" );
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );

        ranking_ref.addListenerForSingleValueEvent( new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child( Gerente.user_name ).exists())
                {
                    int score_user_fb_this_moment=Integer.parseInt(  String.valueOf(   dataSnapshot.child( Gerente.user_name ).child( "score_user" ).getValue()));
                    if(last_score_fb>=score_user_fb_this_moment){

                        RankingModel ranking=new RankingModel( Gerente.user_name,last_score_fb );

                        ranking_ref.child( Gerente.user_name ).setValue( ranking );

                    }



                }else{
                    RankingModel ranking=new RankingModel( Gerente.user_name,last_score_fb );

                    ranking_ref.child( Gerente.user_name ).setValue( ranking );
                }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    } );

    }
}
