package com.developer.souza.ansquiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.BoringLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.souza.ansquiz.GerentePack.Gerente;
import com.developer.souza.ansquiz.Models.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity {
    private EditText txt_name,txt_pass;

    private Button btn_login,btn_cad;

    private FirebaseDatabase database;
    private DatabaseReference user_ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        txt_name=findViewById( R.id.ac_login_txt_name);
        txt_pass=findViewById( R.id.ac_login_txt_pass);


        btn_login=findViewById( R.id.ac_login_btn_entrar);
        btn_cad=findViewById( R.id.ac_login_btn_cad);



        //firebase part
        database=FirebaseDatabase.getInstance();
        user_ref=database.getReference("Users");

        btn_login.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=txt_name.getText().toString(),pass=txt_pass.getText().toString();
                ans_login(name,pass);
            }
        } );



        btn_cad.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cad_user();

            }
        } );



    }

    private void cad_user() {

        AlertDialog.Builder builder_cad=new AlertDialog.Builder( this );
        builder_cad.setTitle("Cadastro de novo usuario" );
        builder_cad.setMessage( "Preencha as informaçoes de usuario");
        LayoutInflater inflater=this.getLayoutInflater();
        View view_cad_user=inflater.inflate( R.layout.cad_user_layout,null );


        txt_name=view_cad_user.findViewById( R.id.cad_nome_user );
        txt_pass=view_cad_user.findViewById( R.id.cad_pass_user );





        builder_cad.setView( view_cad_user );

        builder_cad.setPositiveButton( "Cadastrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String cad_name=txt_name.getText().toString(),cad_pass=txt_pass.getText().toString();

                final UserModel new_user=new UserModel( cad_name,cad_pass );

                user_ref.addListenerForSingleValueEvent( new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //verificando se tem ou nao um usuario com esse nome
                        if (dataSnapshot.child( new_user.getName_user() ).exists())
                        {
                            Toast.makeText( Login.this, "já tem um o usuario com esse nome", Toast.LENGTH_SHORT ).show();

                        }

                        else
                        {

                            user_ref.child( new_user.getName_user() ).setValue( new_user );
                            ans_login( new_user.getName_user(),new_user.getPass_user() );

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError)
                    {
                        Log.d( "meu","Problema para cadastrar user"+databaseError.getMessage() );

                    }
                } );

                dialogInterface.dismiss();

            }
        } ); //final do positive button


        builder_cad.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        } );


        builder_cad.create();
        builder_cad.show();


    }

    private void ans_login(final String name, final String pass) {
        user_ref.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child( name ).exists() && dataSnapshot.child( name ).child( "pass_user" ).getValue().equals( pass ))
                {
                    Gerente.user_name=String.valueOf(  dataSnapshot.child( name ).child( "name_user" ).getValue());
                    Gerente.user_status= (Boolean) dataSnapshot.child( name ).child( "status" ).getValue();

                    Intent home_intent=new Intent( Login.this,MainActivity.class );
                    startActivity( home_intent );
                    finish();
                }
                else
                {
                    Toast.makeText( Login.this, "Nome ou senha errados", Toast.LENGTH_SHORT ).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d( "meu","Problema para cadastrar user"+databaseError.getMessage() );
            }
        } );


    }
}
