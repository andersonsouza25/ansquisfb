package com.developer.souza.ansquiz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.souza.ansquiz.GerentePack.Gerente;
import com.developer.souza.ansquiz.Interface.ItemClickListener;
import com.developer.souza.ansquiz.Models.QuestionScoreModel;
import com.developer.souza.ansquiz.Models.RankingModel;
import com.developer.souza.ansquiz.Views.CategoriaView;
import com.developer.souza.ansquiz.Views.DetalheView;
import com.developer.souza.ansquiz.Views.RankingView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class RankingFrag extends Fragment {

    //coisas da view
    View view_ranking;

    //coisas da recycler_view
    RecyclerView recycler_ranking;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<RankingModel, RankingView> fba_ranking;


    //coisas do firebase
    DatabaseReference ranking_ref,detalhe_ref;
    FirebaseDatabase database;

    public static RankingFrag newInstance() {
        RankingFrag fragment = new RankingFrag();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        //Aqui vc inicia as variaveis que vc vai utilizar
        database=FirebaseDatabase.getInstance();
        ranking_ref=database.getReference("Ranking");



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Aqui vc vai conectar as views que estao dentro do ranking_frag e retornar a view que vc criou

        view_ranking=inflater.inflate( R.layout.rankin_frag,null );

        recycler_ranking=view_ranking.findViewById( R.id.frag_r_rcv_ranking );

        recycler_ranking.setHasFixedSize( true );

        layoutManager=new LinearLayoutManager( container.getContext() );//nao sei explicar direito

        recycler_ranking.setLayoutManager( layoutManager );

        show_ranking();
        return view_ranking;
    }

    private void show_ranking() {
        fba_ranking=new FirebaseRecyclerAdapter<RankingModel, RankingView>(
                RankingModel.class,//1 - vc tem que passar o modelo que vai receber os dados do fb
                R.layout.cell_ranking,//2- vc tem que passar o layout das celulas da view
                RankingView.class,//3- vc tem que passar a classe view, que onde vc conectou os itens
                ranking_ref.orderByChild( "score_user" )  //4- vc passa o firebase ref para poder buscar os dados
        ) {


            @Override
            protected void populateViewHolder(RankingView viewHolder, RankingModel model, int position) {
              //Aqui é onde vc vai setar os dados que vc trouxe do fb nas views
              viewHolder.txt_user_name.setText( model.getUser_name() );
                viewHolder.txt_user_score.setText( String.valueOf(  model.getScore_user() ));
//

                viewHolder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        alert_detalhe();
                    }
                } );



            }
        };

        //Aqui é para atualizar a lista quando mudar algum dado
        fba_ranking.notifyDataSetChanged();


        recycler_ranking.setAdapter( fba_ranking );
    }

    private void alert_detalhe() {

        Intent intent =new Intent( getActivity(),Detalhe.class );

        startActivity( intent );











    }
}