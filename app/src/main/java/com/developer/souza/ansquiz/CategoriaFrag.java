package com.developer.souza.ansquiz;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.souza.ansquiz.GerentePack.Gerente;
import com.developer.souza.ansquiz.Interface.ItemClickListener;
import com.developer.souza.ansquiz.Models.CategoriaModel;
import com.developer.souza.ansquiz.Models.QuestionModel;
import com.developer.souza.ansquiz.Views.CategoriaView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class CategoriaFrag extends Fragment {

    View categoria_view;
    FloatingActionButton btn_add_categoria;
    RecyclerView rcvCategoria;

    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<CategoriaModel, CategoriaView> fbadapter;
    DatabaseReference categoria_ref,quesion_ref;

    FirebaseDatabase database;
    EditText txt_categoria_name,img_categoria,txt_pergunta,txt_alt_a,txt_alt_b,txt_alt_c,txt_alt_d,txt_correta;
    Button btn_valida;

    Switch is_img;


    int id_control=0;


    public static CategoriaFrag newInstance() {
        CategoriaFrag categoriaFrag=new CategoriaFrag();
        return  categoriaFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate( savedInstanceState );

        database=FirebaseDatabase.getInstance();

        categoria_ref=database.getReference("Categorias");

        quesion_ref=database.getReference("Questions");


    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable  ViewGroup container,@Nullable Bundle savedInstanceState) {

        categoria_view=inflater.inflate( R.layout.categoria_frag,null );
        rcvCategoria=categoria_view.findViewById( R.id.rcv_categorias );
        btn_add_categoria=categoria_view.findViewById( R.id.btn_float_add_categoria );

        rcvCategoria.setHasFixedSize( true );
        layoutManager=new LinearLayoutManager( container.getContext() );
        rcvCategoria.setLayoutManager( layoutManager );

        loading_categoria();
        btn_add_categoria.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                create_categoria();
            }
        } );
        return categoria_view;
    }

    private  void userchooice(final CategoriaModel model, final int position){
        AlertDialog.Builder choice=new AlertDialog.Builder( getActivity() );
        choice.setTitle( "Escolha uma opção" );

        choice.setPositiveButton( "Jogar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                next_activity(model);

                dialogInterface.dismiss();
            }
        } );

        choice.setNegativeButton( "Add pergunta", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                create_question(model.getId());

                dialogInterface.dismiss();
            }
        } );
        choice.setNeutralButton( "Sair", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            }
        } );

        choice.setCancelable( false );
        choice.create().show();
    }
    private void loading_categoria() {

        fbadapter=new FirebaseRecyclerAdapter<CategoriaModel, CategoriaView>(
                CategoriaModel.class,//Aqui é a classe modelo dos dados do banco
                R.layout.cell_category,//Aqui é o layout onde esta o rcv
                CategoriaView.class,//Aqui é a classe onde estao setados dos itensviews
                categoria_ref// Aqui é onde vai buscar os dados

        ) {
            @Override
            protected void populateViewHolder(CategoriaView viewHolder, final CategoriaModel model, int position) {
                    viewHolder.nome_categoria.setText( model.getNome() );
                    Picasso.with( getActivity() ).load( model.getImg() ).into( viewHolder.img_categoria );

                    viewHolder.setItemClickListener( new ItemClickListener() {
                        @Override
                        public void onClick(View view, int position, boolean isLongClick) {
//                            Log.d("meu","voce escolhe a categoria"+model.getNome());


                            userchooice(model,position);




                        }
                    } );
            }


        };

        fbadapter.notifyDataSetChanged();
        rcvCategoria.setAdapter( fbadapter );


    }

    private void create_question(final Long id) {

        final AlertDialog.Builder add_question=new AlertDialog.Builder( getActivity());
        add_question.setTitle( "Criar questoes" );
        add_question.setMessage( "Informe os dados das questoes" );
        LayoutInflater inflater=this.getLayoutInflater();
        View view=inflater.inflate( R.layout.create_question ,null);

        txt_pergunta=view.findViewById( R.id.txt_title_question );
        txt_alt_a=view.findViewById( R.id.txt_alt_a );
        txt_alt_b=view.findViewById( R.id.txt_alt_b );
        txt_alt_c=view.findViewById( R.id.txt_alt_c );
        txt_alt_d=view.findViewById( R.id.txt_alt_d );
        btn_valida=view.findViewById( R.id.btn_valida);

        txt_correta=view.findViewById( R.id.txt_correta );
        is_img=view.findViewById( R.id.is_img );
        txt_pergunta.setHint( "Digite a pergunta" );



        is_img.setTextOn( "Sim" );
        is_img.setTextOff( "Não" );




        btn_valida.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(is_img.isChecked())
                {
                    //Toast.makeText( getActivity(),  "Cole o link da imagem no campo da pergunta", Toast.LENGTH_SHORT ).show();
                }


                final String pergunta=txt_pergunta.getText().toString(),
                        alt_a=txt_alt_a.getText().toString(),
                        alt_b=txt_alt_b.getText().toString(),
                        alt_c=txt_alt_c.getText().toString(),
                        alt_d=txt_alt_d.getText().toString(),
                        correta=txt_correta.getText().toString();


                if (correta.equals( alt_a )||correta.equals( alt_b )||correta.equals( alt_c )||correta.equals( alt_d ))
                {
                        quesion_ref.addListenerForSingleValueEvent( new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                QuestionModel questionModel=new QuestionModel( pergunta, id,

                                        Integer.parseInt( String.valueOf( dataSnapshot.child( String.valueOf(  id )).getChildrenCount() ) ),

                                        alt_a,alt_b,alt_c,alt_d,correta,is_img.isChecked());

                                        quesion_ref.child(String.valueOf(   questionModel.getId_categoria() ))

                                                .child(String.valueOf(   questionModel.getId_questao() )).setValue(questionModel );
                                limpaTexto();
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        } );

                }
                else
                {
                     Toast.makeText( getActivity(), "A resposta tem que ser uma das opções", Toast.LENGTH_LONG ).show();
                }
            }
        } );


        add_question.setView( view );
        //se o usuario escolher uma img como pergunta, no campo da questao ele deve informar o link da img
        //exemplo:data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhMVFRUVFRUWGBYVFRUVFhYVFRUWFhUVFRUYHSggGBolHRYVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0lHSUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS4tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xABAEAABAwIEAggCBwcDBQEAAAABAAIRAyEEBRIxQVEGEyJhcYGRoTKxBxRCUsHR8BUjYnKC4fEWM5I0NXOzwib/xAAaAQACAwEBAAAAAAAAAAAAAAAAAQIDBAUG/8QAJREAAgICAwABBAMBAAAAAAAAAAECEQMhBBIxUQUTIkEycYFh/9oADAMBAAIRAxEAPwDmelFCcISdKsARpQhKIRgIATpR6U81qDmp9RWMQhCWQjhKhiIQhLhHCKAbhP0MOCJedI4cSfAchz/vD2Fwgf8Aa5CIJMmwsFuejnQzQRVruHIMG0cSXH5d5SYGPwWQ1KznMpgy2JkEiTtBiIV1lnQ15c01HBoMiJkwW3BtvLvbdb9lME6WCGNA242BA23iJ8YUXMK19Lftdny+0fS0fxKNjMrT6MUWtcCXOM9pwkAwDEbjjPsOSj0uh0NdNWxd2ZYZ4giWk8DvESAtbSoOAgRIBJt95zTbzHoEVfskA7TAAvYSfWBv3osZjcR0JqQ/S4GCNNwJ5l0xFuF+PK9Piej9amCXN2gyAYuYiec/IrqNMEMMTPGe/kP1cd6Onh7du5jjwlFio49WoFpg7iJHInge9I0ro+a9G6FW7G6Hby0kgnjqby/JY3N8qqUXhjm30zIiCJPai0BSTsKKtjbqywgUBrYMFTsOU6EO1GqOQpJNkw9AxohFCWUkoAUAjSQUaABKCEokCGhSQNNSmsROpq/qZ/uFfUaktUqoxNaFBxLU7DalEJsp6mELegetjBCUGpdQJGpQemSW0EWoNaSjF1segXR/rqoqvg06cOIIBDncBPKU26VsaVujSfR/0XFOmK9UDW8DSI+FlzPibegWoZQ7TidiZiOAnYd6kUQSZ4KwbCxvLbNSw0UD8MQ0xveT/EePzVfisFDp56gI4cBHkAtVVw4I5Smn4KSCTtdPuJ4zOsoFpA77+EfmUoYUxMXNvUk/itAcG1H9WbwSeQaxopzhhIHAfn+Q/FRsTQLi4Xgx593hMK/qMEbKLVCi8rJLCimxGFBAbewgx7AR+rBVea5Ox7DTkg7i7TB5XFwtDiHk7b81X1KjuNk4ZbZGeKkcvx2V1KTnCowtg2MGIsZaSPhv6phhXSMZh21Boqt1cibQdrRt8lgs1wfU1Czhw8FqjKzO1RHDklyDSicVMQgpJSiklIQQKOUOrPIpKBipRpCCALENhE4hRcRXIUX6yr3kSMixN7JFVNJPWSjBUeyZao0hDgn6ATLlMwTZTgvyFkf4kfEtUVokqzxlOyhYR0PaeRB9CoZFTJYnaNB0S6KvxjyNXVtAkujUeFg3gfHvXXcrylmEoNotgkSXOiNR7/KBfkqjoEyjTpnqyCXu1TJkg/CPLaOBlXeLxHaPhHusuaeqNWGFysWDsnqb1EDuR/HySm1B/lYmb6J/WIOeoX1gTEoOrJ9iPUkuqJJqKL1kiSja8IsOo+5yg1XXTtWqq/EPInj+FkMEhNd/JQjXvHLjxA7k1UrTeY3n05KM5xcLHtC4KjdMn1H8TTtIb58flZZvpVhw6n1g3Bg9w/yf1ZaTCVSRpi/EbGVAx9AOY7cyLjnx81sxy8ZiyQptHP2oEpVQQY/BNuK0mYSVPy3B6iO9V5K0vR8iQeQQgLJuQ9nZZzNcDpJ7l0ZtYRPCFjukTwTPcUwMmglSjSAtcbgQqithYVji8ykqI+sCjTKLkmQDZO0jKbruR4Z10IuWx2q1TMuKZcyVY5fhbK/ErZTnaURnHGyqsOJdHz5K2zNsKopPh0+vh4JZlsMDuJ1XoGwglo/24aJ4klrXesb+LVoqnxHxVd0PpuNIEmYYwnaxeNXrpNP5KyYwXJvJ2G58PRczMzqYEOdYDYE24iISW0XgyIcO6x9DY+qNz3bDSyOBMn0GyNlYtPbAH8QuCqLNVC3OG15G4gpp9QbQFJdU5JiowEiQk0CEvqgckTqjRy9k86m0D4R6JrEMb9xp/pB/BFMNEKtjWTE+huqzFYsTDXHzg+0BWGJpuA3a3ui3cqmoXAm4PKCe/mk7JpIZr17RMx5JvCVCLlQcViO0QRfj+aW+qYN4HAd44lRHRaYQzUB7lNcyCZB8YlVuUtLoP6/wp+NrAW7v0PkteL+JizfyMBntLTWeALTvfj5qvp0y4ho3KuOklUOeDMGCOYIBMeH9lAyyoGvkrYvDE/S1w3R0uHFWGEyrq7QbcVaZbj2Rc8E6/MmXCkIi06DiIvCi5jkhcI4lWlLMGgKT+0mJgY7/AEq7v9EFsf2mxBKgORuZJRFsJ0FIqFQKe1kaoUeGPaSaicwTZcEFyLqlT2Vxg4AVW3ZSMCSStOBmflrQznUKlo4cuO3vE9w4k+C02MweoKqZTLLCQSdxZ3hPK6snBydlWHIoqjrHRrEBtB1V/YboDiSIbqbqD3AxcmB7BQx0lptklj4kibAmwNmz38VkmVR1bWh1W9ZoLesf1ZinVqdqnMEzSbuN0WY1CKcgSSY2MgjlG1p9lx89xlR3eNFSh2NUelOGdZr2tP8AG0i/KWBw9U5+0nnYCo3aaZ1+RAlc2bhK5PZLKfgC43+9NioOL+tMPa6tw7paVTTZocVHw65hs4ZBExHA2KlnG7HvC5RlmeVnOax9R4DYBHZc0t5FrgWk8jE895HQa2KrNbqDBUbuC4MkjuDCxVO0yxJMucXmIABJgFwHf6KtxeZveTpLWAcXXPoFlcw6SU6khzXNLTfttZpPCQ9vtN4UJuLoF3/VgTwfSMeE658w1PbBRivfTTVHNJ7dZ7ze1gPIR80cHcEvb5aoHhuoGGr4cEOGIpzzArNBjjOmAduO887WAxev/bdTqEj4ab2ufck/ADPLYKXUg5FVidN3Anune24KhGoXuA38O7cp/FOLnQBeSDwgjeeSRTDZhvAETztwUaJl9lbgxt/1+pVPnmYC7dUOJJiYIBi3cpmMqFoDW/Edh7ybbXHyWUz2i3r3vvJaJvbxB7wNvFao3pIx0n2b+GRsVWJsefp5KLqRSiK2GAdGLeNnFL/aNT7yiIApiJf7RqfeSW414+0VHCNAEv8AadTmgokIkAEkkIByepMlRbKkiHUYp2XUeKPqArLB0rKDZfD0Q90KyyqlxTFfLS4WVnl1AtFwtmBqjJy0/wDC1ZgdQ2VJnOVuZ2gFscrcCIUrH4dpbBUvuSjIrWKMo2ZFuWacvbiOIrtfbgztUZPcOscfBQn1m2aQ6WgbRE8JnuWzyZ9N1B+GdBEPBaeLHz7XIWVqZM6nOol7Bs8XO/2wOI2nYxw2XH5VvKz0XB6rEkQaryOR5GwMeI39FV4yqeVv5v7Kz0sJLdR8Z4eCjVsLSaZJc7jvYDwCzbN1IqMJhyXNgOJc5rWgbklwA0+ZFxtc8F1Y9H6NGlLWgODbu2JgXLiFC6O9HNLmYh8agOwyP9uQRc8XQT4T3K9z1+mke8QpLa2USf5Uji2d1HvxLuLRNxvAE7nj3+CTQysVgS1sCI1PlxPmZU/Mcsd1rnXDahtNocWxE+Q9VCwdUsGlziC21x7neVPtS0KOPs3Ywej/AFYJFUeBIPtCTRxEnTUaW8AWiWlWjsSCLlp9G/Kx80WKY1rJc1x7wW2HeAl9y/SX2uvhJy7MCXGnVfbTINR4Fm/CzU47X27lYYd0ukFrjP2SS0RzNp8B6rO4AMkFpmLknvtHurOhi9LrcihVYpJ1okZtmxY4U93kguJ7+A8BAjxVdm5PZJJJdfcx5Dxn1U9uF1FlZ1yeyBckuk+0KJn9nAA2FiO8TdX4PylZk5VQx0irCNIlKlbDnBFJlGU/h8G9/wAIQAxKUl18K5nxBNSgBUoIpRoAjBylUaqjObCNijQicayscurQb7KnphWGGRRDs7Nfhi0hS2NCy1Gu4bK3wOLJ3SVxdlnZTVMtxLbtTWLzq0HdWOFZqCps+ywxIC1wyxl6ZpYZQ3Eoa+Oc+q3QSCXASCQbkCF0GnUDTtZc3yqgfrFIG37xp9DP4Lb4ypDJ2aPLZYvqTSlFL4On9KTlGTfySsa3CkEvpt+XHjCzzMfRr124fDUmNP2qkA9W0fdnjwjmVS4jEVsW4spWbMOedhzA5la3I+jzMPThvxESXAdoujme8/Nc9Jv060ukP7NLl2H6tuiSQOdz5pjNXdk/JVtbN30fjFjYHgYVNmnSVgbc3i3mE7RWsbeynzt1Rxuw9SHDUQYIAIkjiY/BO/UX0gCWCrT4PaJkbgnvuqvEdJi5oYwE3nayd6FZ+5s0Hz2ZLf5eV+XySa0WL4JNSpTN2MGr+T+wKqq7XOJLWQRuQYB4bc1p83xTHNl7Wk84BPuqJlZrnQ3ZQ0TfZIrhR0B1hJMmNuMBIeSBtcqxxdOJTuVVaQnVBgkX3t3Kd6Kk9k/A1wKdhZotO+qLrM5viQ920K4zCvq2ADeAFvXvWaxjrro4cLhC5es5HJ5Eck6j4hAKkYeiXnS0SSojStr0Uy8Busi5+SsSKBnA9GZA1XJW1yvo41rNk/ltAblaHDxpU/AOcdLsohpICxAwb/uldxzHACoIhQD0fZq2SasDjn1Kp90+iNdo/YLPuoIoDiZoyU4zCqxpUQlvgKLRV20Q6VGFYUKahOqgJynilFoXYtGUwrLA0pVAMWp+XZzTYe263cJScHRPHPZvMqoWU3E4QOEQsn/r/DUx2WVHH+lo9yoGL+lD7mHH9TyfYBRUWjS5ot35UGVWujZ0p3MKJfTcxtySffisTX+kWu9zZp02t1CYBnTIm5PJdAYLzOyzcq7VmzhySTozeTYeoym2mwiWOcCLCSHG59Ve5bnbnVOrqMIdeLtJc0HtOaBwuBCXkeGs5x+09x94HyUs4FpcHQJFwSBY7SORufVVY5P9l+VJtkHMMdRqSwkTc6XCP5QQ6/8AhZ3NOjVJw1B7W/ETJkfwiOd1rcwwepul7WvtAmxEcjuCsziejtNxkipO5/eOg98Kb9FHzRTnKNAiBMtEg2k8Bz4eqzmOGlwq04Dmu4fj3XV/j8qqatLKhBl0m7t2hluXZDfRQ8J0VqB7SahcNQJDrgydoQ2khxUrHalc1aYfETw5HiFBwrS2rA2ifzW2qZYKdN7iAAYgctP69ljXPmo4jlA/XmqEartFg46nAc4Up1Mch+KqamJNJra5uwVBSdG41NJB9lch7XtDmmQdiF1uFBKN/s859SyvvX6K/FCyz2MF1o8Zss7iz2lozMy8cZpi66VkZGhoHILmwW76JVtQBPBVw2aWbvDUoAUmpiIESq9+KhqqXY8uPmp9SNmyoVbJfWALM/tQAC6cZnLXbFHQLL/rygqb9pDmiS6BZyb6807T+aZqV5USUReoh1Q86om3VymXOSCUhpDjqpTDnIyUlAxJPelNYXJTWJwFKgDbhm8brquRYlz6NFzgQXsG4iQJaXDmCWm65xkuXuxNelh2b1ajWTyBPad5CT5Lt3SzD02ClTpjSKTQxoHBjRYeUe6z8mKcLNXEnU6+QqNEBsNTNakd5TWWY3UIJuPkrFzhE8Fjo3bsp674F3DzKr31nCSII8fktNV0xsD4iVTY3DUybNHyj0SaZZGS+CgrVnPfGnxUotiCTxHuQJU7D4emDI2H63VRm+Na0b3JHsZhQ2TVXoi9M80P+23lf8FmKLZIHmU5meJ62qSLj9Qo2ZPFGg8/aIgeLrD0lSSthOSjEkUWdblGNqb6cRTcPAPa35FZXB46pT+B5HyPiFvejeF//P4w8wXf8XA/gucsXShrw4eTb2aHDdIesGmoIPMfko+JcCZCqHsm/FO4asdirHJv0qjBR8JzStFkmYaAs4y6daSERdMmb2pnYc2AUy3HADdYjUeaBqnmVP7hHqaTMs4gQHKqoZxUaZBVYjBUXNjouf8AUFXmgqdGjuwpB6kRKSCgSgASilFKKUAGUSSw/NLSAUjBSQjTA6r9CuRDU/HVYAANOhqI7Tjaq8A8h2Qe94Wg6S1pqHzt+vL0TXRpjamHwegdg0qIjeIAa8Hv1B096azRxe9zjvJ9zKy53aNfHjUrMT0Txp+t4mkXmNWpgJsPvAeMhaXEZw+kbg+hI7isfRw5o5i15s15P4gj3C3eLwoqMAJi1jN2/mFil6dCD+SsPSLVdpEExv8Aq6huzhkuL7kHv9+azmb4B1KpBh245W5gjZN4bLHnQ/UQ109ngIJFvzSTsvpI0WLzwFpbTBEm5KpK5L+J/L1Tpba3kjbTkRH90WQb+BnDUAPhHnzVL0wP7tsHZ1x5GFe4kw2B6LOBvXV2Uj9pwnwFz7AqzF7ZnzbjR0bBYTqejtdpEHqTPiY/ErjoXcumX7rIqo+8KLf+VSnPtK4cuhHw5U/RbUzU1A2ThSK3AjdMiScPiPVTmulVlFseKkscmBLKTCSx6cEFAxCCUWpKQByjSUEAG1AlEAieVMQEEkBF6pAELFLD+5ABEkMVJRHxRhIrOgSmB036E8fXe+rhg3VRYOtDpjqqhPwjmHQTHAtJ4lbPOMNFQiNySPAqZ9GPRv6jgWBwitV/e1eepws3+kQPVT+kOGHZfebiOHNU5I2i3FKmYHpdlI+rioB2qZDv6SQHD9clOoOBYHbgwe8SJVl0jpzhK3/jd7BZTCZgG4NzgCS2RAvNrGPMLDnVM6PHblH/AEpM6q665jZpsl0Qer0hpOkkze4cSUMuyqpVdq2aZlx9dlp6GCbTbBuBx71TWjU5fozNKi47tcBvMQluMDYgfNXOMqSdIEgWUejS1apbqAsAOfPvTIMzeYOtA5bhDoBlDq+LJaJ0NieALjAJ8gVbDo5UrmGiDy3/AMLqvQvowzBUAwQXHtVHx8Tzv5DYdwWnBGzJyJKK/wCmI+nEijgMPQb9qs2eZDGPPz0riS6J9OubGrjadEfDRpzH8VQ8vBo9VzsLajmsCcDLJNNOhMQTUsJGxTgTGKDk41yaSmoEPNcUdj3JsJUpgHpRpMo0UARKSlFIlABlEAggkMNBEUYQAa0P0eZH9dzClTImnSPXVOUMILQfF0e6zzjAldp+gbL2swlSuWxUq1DJO+hghgHddx80MEdNdZQcxp64aBMXJ5ch4lSKl3QlvbAsoMktGXzHC6qVRn3mOHmWkLn3R9ga14I2iZ8Sut46hIkcd/FcvNHqcQb2bUIiN2zb2IWTkrxnQ4cvUXFLB6QI4z/ZO1KbQ2CN+CmGkQ20G1vndQ/q7jB4j091nqjRdkJmGE6dzxHd+oU7K8rLzpYDHEkWAU7B4DXL3wxo+J2w8Aeak5j0gp0qejDC8gBxFhO5g3LvH3VuPHe34U5MrWo+kzE4mhgqdxLonSI1O5Fx4DvPksnR6W4t9ZpBGkuDRSA7MExHOe9Q8RUNR0ElxJkncknckrMdOc4q4JjBQIY95PbAlzRF9JOxvutNN+eGZ9Y7ltmT6f4rrMyxTpn97o/4NDI9WlUQTTTxNyblKBVyMg/STiRSS0xBO2SqZkAqP8Zj7I37+7wUkIGKRgpsuRB6YD2pHrTJKGpAh/UgmdaCAH3FICMokDDQCJGEAGlBEAjTAXhcI6vWp0G3L3gW5Tdeoshy5uHospNEBjAPPifWVxb6Esm67FvxLh2aIhv87tva67yN/JRYwqIuSjqBGwQECogN6ZEFc06Y4TRiTPwvAP4W9PddO0rK9OcEHsZUIktOk+DtvcD1VOZXE08afXIipyjEAsA+7bwHBSq8NGpxIHAA3PhyHf8ANQsswvVDWdiBpbz5OPdy5+G6MU4vNzdU48V7ZpyZEnURrF419Qy6zRZrRs0dw59+6rqlQukclYdVNglU8uc55DRMxsPmr1Epc0lRFy3DGZK579LDya9NnJpPuF2zB5IQO0QO4XXJ/pkwjKeKohkyaTi6Tv2gG+GxVpmkznDKRUilThGUoKRWAhM1QTYbcT3J5AIAJrYEBAo0ioCgBBci1JslEgB7Wi1pslILkWBI1o1FlBKwLQoFBAqQBpQRBKCAAmsS+Ank5k2BOJxVKgPtPAPhN02B3n6Ico+r5fTJEOqk1Hc72b7BbfimcDhxTptY3ZrQ0eAEJ4qBIUiQQSEBV+flnV6HAEuLbfykOk90jzVgTFzsLqn+rurPL3WHDuHJJkl7ZQVGlxKep5Q50WieJ4d60dLBNbsPPinCzteA/XyRRLsUxypjAABPMnc/krChRDRDQB4Jyq1LpNRQrBC4N9MlWcwDfu0Ge76h/Jd9qCy89/Sy6cxqd1OkPYu/+k0RfhiiUoJBSmqREMoSieEjXG6AHJSKr4SwVFxJ4IAbZslBElBIBL0hLcm3JADUgkSggC4KNGgpgG1KRIIANaH6Kv8AudLxKCCJAj0o3ZAoIKCJACUESCAGsV8BRYTZBBAfoccmvtHwH4oIIEhqqlUkaCCQdbZed/pS/wC5V/Cn/wCpqCCa9E/DGlKCCCZEBSX7FGggAmqJU+JBBJgBLYjQQgEOTdRBBJgNIIIJAf/Z

        add_question.setNegativeButton( "Sair", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        } );

        add_question.create().show();


    }

    private void create_categoria() {

        AlertDialog.Builder alert_categoria_add=new AlertDialog.Builder( getActivity() );
        alert_categoria_add.setTitle( "Adicionar categoria" );
        alert_categoria_add.setMessage( "Informe os dados da nova categoria" );
        LayoutInflater inflater=this.getLayoutInflater();
        View view_add_categoria=inflater.inflate(R.layout.add_categoria ,null );

        txt_categoria_name=view_add_categoria.findViewById( R.id.txt_name_categoria );
        img_categoria=view_add_categoria.findViewById( R.id.txt_img_categoria );



        alert_categoria_add.setView( view_add_categoria );


        alert_categoria_add.setPositiveButton( "Cadastrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, int i) {

                final String name = txt_categoria_name.getText().toString();
                final String img_link = img_categoria.getText().toString();

            // user_ref.child( user_Model_new.getUser_name() ).setValue( user_Model_new );

                categoria_ref.addListenerForSingleValueEvent( new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final CategoriaModel categoriaModel = new CategoriaModel( dataSnapshot.getChildrenCount()+1,name, img_link );

                        categoria_ref.child( String.valueOf( categoriaModel.getId() ) ).setValue( categoriaModel );

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d( "meu","erro ao adicionar a categoria"+databaseError.getMessage() );
                    }
                } );

            }
        });

        alert_categoria_add.create().show();

        alert_categoria_add.setNegativeButton( "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {



                dialogInterface.dismiss();
            }
        } );





    }

    private void limpaTexto(){
        txt_pergunta.setText( "" );
        txt_correta.setText( "" );
        txt_alt_a.setText( "" );
        txt_alt_b.setText( "" );
        txt_alt_c.setText( "" );
        txt_alt_d.setText( "" );

    }

    private void next_activity(CategoriaModel model ){

        Intent play_game=new Intent(getActivity(), PlayActivity.class);
        Gerente.categoria_id=Integer.parseInt( String.valueOf(  model.getId() ));
        Gerente.categoria_name=model.getNome();
        startActivity( play_game );
    }
}
