package com.developer.souza.ansquiz;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class Online_frag extends Fragment {

    public Online_frag() {
        // Required empty public constructor
    }

    public static Online_frag newInstance() {
        Online_frag online_frag = new Online_frag();
        return online_frag;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.online_frag, container, false );
    }

}