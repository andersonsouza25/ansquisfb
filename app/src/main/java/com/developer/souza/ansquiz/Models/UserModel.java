package com.developer.souza.ansquiz.Models;

public class UserModel {

    private String name_user;
    private String pass_user;
    private Boolean status;

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getPass_user() {
        return pass_user;
    }

    public void setPass_user(String pass_user) {
        this.pass_user = pass_user;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public UserModel(String name_user, String pass_user) {
        this.name_user = name_user;
        this.pass_user = pass_user;
        this.status = true;
    }

    public UserModel() {

    }
}
