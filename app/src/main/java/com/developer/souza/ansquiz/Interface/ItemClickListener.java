package com.developer.souza.ansquiz.Interface;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);

}
