package com.developer.souza.ansquiz;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.souza.ansquiz.GerentePack.Gerente;
import com.developer.souza.ansquiz.Models.QuestionModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btn_a,btn_b,btn_c,btn_d;
    private TextView txt_question;
    private ImageView img_question;

    private FirebaseDatabase database;
    private DatabaseReference ref_question;
        String questao,alt_a,alt_b,alt_c,alt_d,correta;
     static    int ponto=0,questao_atual=0;
        boolean img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_play );

        btn_a=findViewById( R.id.ac_p_btn_a );
        btn_b=findViewById( R.id.ac_p_btn_b );

        btn_c=findViewById( R.id.ac_p_btn_c );
        btn_d=findViewById( R.id.ac_p_btn_d );
        img_question=findViewById( R.id.ac_p_img_question);
        
        txt_question=findViewById( R.id.ac_p_txt_question );
        
        btn_a.setOnClickListener( this );
        btn_b.setOnClickListener( this );
        
        btn_c.setOnClickListener( this );
        btn_d.setOnClickListener( this );
        database=FirebaseDatabase.getInstance();

        ref_question=database.getReference("Questions");
        
        carrega_question( Gerente.categoria_id );
                
    }

    private void carrega_question(int categoria_id) {

        ref_question.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if ( dataSnapshot.child( String.valueOf( Gerente.categoria_id )).exists()){
                    for(DataSnapshot post: dataSnapshot.child( String.valueOf( Gerente.categoria_id ) ).getChildren())
                    {
                        QuestionModel question= post.getValue(QuestionModel.class);
                        Gerente.questionModels.add( question );

                    }
                    show_question(0);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        } );


    }
    private void make_point(boolean acerto){
        if(acerto)
        {
        ponto=ponto+10;
        }
        else
        {
            ponto=ponto-5;
        }

        if (ponto<=0)
        {
            ponto=0;
        }

    }
    private void show_question(int questao_atual) {
        if (questao_atual<Gerente.questionModels.size())
        {

            questao=Gerente.questionModels.get( questao_atual ).getQuestion();
            img=Gerente.questionModels.get( questao_atual ).isImg();

            if (img)
            {

                 Picasso.with( getBaseContext() )
                 .load( Gerente.questionModels.get( questao_atual ).
                 getQuestion() )
                 .into(  img_question);

                txt_question.setVisibility( View.INVISIBLE );
                img_question.setVisibility( View.VISIBLE );


            }
            else
            {
                txt_question.setVisibility( View.VISIBLE );
                img_question.setVisibility( View.INVISIBLE );
            }

            alt_a=Gerente.questionModels.get( questao_atual ).getAsw_a();
            alt_b=Gerente.questionModels.get( questao_atual ).getAsw_b();
            alt_c=Gerente.questionModels.get( questao_atual ).getAsw_c();
            alt_d=Gerente.questionModels.get( questao_atual ).getAsw_d();

            correta=Gerente.questionModels.get( questao_atual ).getCorreta();

            //setando texto
            //Log.d( "meu",""+questao );

            txt_question.setText( questao );

            btn_a.setText( alt_a );
            btn_b.setText( alt_b );
            btn_c.setText( alt_c );
            btn_d.setText( alt_d );

        }
        else
        {
            fim_game();
            //clean_data();
        }
    }

    @Override
    public void onClick(View view) {
        Button btn=(Button) view;

        if (btn.getText().equals( correta )){


            questao_atual=questao_atual+1;
            show_question(questao_atual);
            make_point(true);

        }else{
            Toast.makeText( this, "Errou", Toast.LENGTH_SHORT ).show();
            make_point(false);
        }

    }

    private void fim_game()
    {
        //        Bundle data_send = new Bundle();
        //        data_send.putInt( "SCORE", score );
        //        data_send.putInt( "TOTAL", total_questions );
        //        data_send.putInt( "ACERTOS", corret_alt );
        //        fim.putExtras( data_send );

        Intent fim=new Intent( this,Fim.class );
        Bundle data=new Bundle(  );
        data.putInt( "ponto",ponto );
        fim.putExtras( data );
        startActivity( fim );

    }

    public static void clean_data(){
        Gerente.questionModels.clear();
        Gerente.categoria_id=0;
        ponto=0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //O método onResume é acionado quando a Activity se inicia e quando é reiniciada.
    }

    @Override
    protected void onPause() {
        super.onPause();
        //O método onPause é acionado, quando a Activity deixa o primeiro plano.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //O método onDestroy é chamado quando a Activity vai ser destruida.
    }

    @Override
    protected void onStart() {
        super.onStart();
        //O método onStart é executado depois de a Activity ter sido enviada para o segundo plano.
    }

    @Override
    protected void onStop() {
        super.onStop();
        //O método onStop é chamado quando a Activity não está mais visível para o usuário.
        clean_data();
    }


}
